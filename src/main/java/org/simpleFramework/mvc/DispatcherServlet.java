package org.simpleFramework.mvc;

import lombok.extern.slf4j.Slf4j;
import org.simpleFramework.aop.AspectWeaver;
import org.simpleFramework.core.BeanContainer;
import org.simpleFramework.inject.DependencyInject;
import org.simpleFramework.mvc.processor.RequestProcessor;
import org.simpleFramework.mvc.processor.impl.ControllerRequestProcessor;
import org.simpleFramework.mvc.processor.impl.JspRequestProcessor;
import org.simpleFramework.mvc.processor.impl.PreRequestProcessor;
import org.simpleFramework.mvc.processor.impl.StaticResourceRequestProcessor;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * 1:拦截所有请求(@WebServlet("/")可以拦截所有请求，×.jsp请求除外)，
 * 2：解析请求，3：派发给对应controller的方法里进行处理
 *
 * @author: air
 * @create: 2020-03-14 10:49
 */
@WebServlet("/")
@Slf4j
public class DispatcherServlet extends HttpServlet {
    private final List<RequestProcessor> PROCESSOR = new ArrayList<>();


    @Override
    public void init() throws ServletException {
        // 初始化容器
        BeanContainer beanContainer=BeanContainer.getInstance();
        beanContainer.loadBeans("cn.airfei");
        new AspectWeaver().doAop();
        new DependencyInject().doIoc();

        // 初始化请求处理器
        PROCESSOR.add(new PreRequestProcessor());
        PROCESSOR.add(new StaticResourceRequestProcessor(getServletContext()));
        PROCESSOR.add(new JspRequestProcessor(getServletContext()));
        PROCESSOR.add(new ControllerRequestProcessor());

    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 创建责任链对象实例
        RequestProcessorChain requestProcessorChain=new RequestProcessorChain(PROCESSOR.iterator(),req,resp);

        // 通过责任链模式来依次调用处理器对请求进行处理
        requestProcessorChain.doRequestProcessorChain();

        // 对处理的结果进行渲染
        requestProcessorChain.doRender();

    }
}










