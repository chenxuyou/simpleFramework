package org.simpleFramework.mvc.processor.impl;

import lombok.extern.slf4j.Slf4j;
import org.simpleFramework.aop.aspect.DefaultAspect;
import org.simpleFramework.mvc.RequestProcessorChain;
import org.simpleFramework.mvc.processor.RequestProcessor;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;

/**
 * @description:
 * @author: air
 * @create: 2020-05-14 14:38
 */
@Slf4j
public class  StaticResourceRequestProcessor implements RequestProcessor {

    public static final String DEFAULT_TOMCAT_SERVLET = "default";
    public static final String STATIC_RESOURCE_PREFIX = "/static/";
    private RequestDispatcher requestDispatcher;

    public StaticResourceRequestProcessor(ServletContext servletContext){
        this.requestDispatcher=servletContext.getNamedDispatcher(DEFAULT_TOMCAT_SERVLET);
        if (this.requestDispatcher==null){
            throw new RuntimeException("there is no default tomcat servlet");
        }
        log.info("the default servlet for static resource is {}", DEFAULT_TOMCAT_SERVLET);
    }

    @Override
    public boolean process(RequestProcessorChain requestProcessorChain) throws Exception {
        // 通过请求路径判断是否是请求的静态资源(webapp/static)，如果是则转发给default servlet处理
        if (isStaticResource(requestProcessorChain.getRequestPath())){
            requestDispatcher.forward(requestProcessorChain.getRequest(),requestProcessorChain.getResponse());
            return false;
        }
        return true;
    }

    private boolean isStaticResource(String requestPath) {
        return requestPath.startsWith(STATIC_RESOURCE_PREFIX);
    }
}








