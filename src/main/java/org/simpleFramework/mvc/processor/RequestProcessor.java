package org.simpleFramework.mvc.processor;

import org.simpleFramework.mvc.RequestProcessorChain;

public interface RequestProcessor {

    boolean process(RequestProcessorChain requestProcessorChain) throws Exception;
}
