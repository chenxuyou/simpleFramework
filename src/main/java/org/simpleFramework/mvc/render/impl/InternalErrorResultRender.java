package org.simpleFramework.mvc.render.impl;

import org.simpleFramework.mvc.RequestProcessorChain;
import org.simpleFramework.mvc.render.ResultRender;

import javax.servlet.http.HttpServletResponse;

/**
 * @description:
 * @author: air
 * @create: 2020-05-14 14:56
 */
public class InternalErrorResultRender implements ResultRender {
    private String errorMsg;

    public InternalErrorResultRender(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Override
    public void render(RequestProcessorChain requestProcessorChain) throws Exception {
        requestProcessorChain.getResponse().sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,this.errorMsg);
    }
}
