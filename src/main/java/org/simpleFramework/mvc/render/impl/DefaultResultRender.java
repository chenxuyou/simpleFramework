package org.simpleFramework.mvc.render.impl;

import org.simpleFramework.mvc.RequestProcessorChain;
import org.simpleFramework.mvc.render.ResultRender;

/**
 * @description:
 * @author: air
 * @create: 2020-05-14 14:54
 */
public class DefaultResultRender implements ResultRender {

    @Override
    public void render(RequestProcessorChain requestProcessorChain) throws Exception {
        requestProcessorChain.getResponse().setStatus(requestProcessorChain.getResponseCode());
    }
}
