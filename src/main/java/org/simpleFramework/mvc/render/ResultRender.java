package org.simpleFramework.mvc.render;

import org.simpleFramework.mvc.RequestProcessorChain;

public interface ResultRender {
    void render(RequestProcessorChain requestProcessorChain) throws Exception;
}
