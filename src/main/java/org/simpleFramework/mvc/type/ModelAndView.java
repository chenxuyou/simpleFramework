package org.simpleFramework.mvc.type;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

/**
 * @description:
 * @author: air
 * @create: 2020-05-16 11:24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ModelAndView {
    private String view;

    private Map<String,Object> model=new HashMap<>();

    public ModelAndView setView(String view){
        this.view=view;
        return this;
    }

    public ModelAndView addViewData(String attributeName,Object attributeValue){
        model.put(attributeName,attributeValue);
        return this;
    }

}
