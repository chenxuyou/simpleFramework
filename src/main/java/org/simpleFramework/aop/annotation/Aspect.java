package org.simpleFramework.aop.annotation;

import java.lang.annotation.*;

/**
 * @description:
 * @author: air
 * @create: 2020-05-09 15:26
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Aspect {
    String pointcut();
}
