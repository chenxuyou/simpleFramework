package org.simpleFramework.aop.aspect;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.simpleFramework.aop.PointcutLocator;

/**
 * @description:
 * @author: air
 * @create: 2020-05-09 15:40
 */
@AllArgsConstructor
@Data
public class AspectInfo {
    private  int orderIndex;

    private DefaultAspect defaultAspect;

    private PointcutLocator pointcutLocator;

}
