//package org.simpleFramework.aop;
//
//import org.simpleFramework.aop.annotation.Aspect;
//import org.simpleFramework.aop.annotation.Order;
//import org.simpleFramework.aop.aspect.AspectInfo;
//import org.simpleFramework.aop.aspect.DefaultAspect;
//import org.simpleFramework.core.BeanContainer;
//import org.simpleFramework.util.ValidationUtil;
//
//import java.lang.annotation.Annotation;
//import java.util.*;
//
///**
// * @description:
// * @author: air
// * @create: 2020-05-09 16:10
// */
//public class AspectWeaverbak {
//
//    private BeanContainer beanContainer;
//    public AspectWeaverbak(){
//        this.beanContainer=BeanContainer.getInstance();
//    }
//
//    public void doAop(){
//        // 1.获取所有的切面类
//        Set<Class<?>> aspectSet= beanContainer.getClassesByAnnotation(Aspect.class);
//
//        // 2.将切面类按照不同的织入目标进行切分
//        Map<Class<?extends Annotation>, List<AspectInfo>> categorizedMap=new HashMap<>();
//        if (ValidationUtil.isEmpty(aspectSet)){
//            return;
//        }
//        for (Class<?> aspectClass:aspectSet){
//            if(verifyAspect(aspectClass)){
//                categorizedAspect(categorizedMap,aspectClass);
//            }else {
//                throw new RuntimeException("框架中一定要遵循给Aspect类添加@Aspect和@Order标签的规范，同时，必须继承自DefaultAspect.class" +
//                        "此外，@Aspect的属性值不能是它本身");
//            }
//        }
//
//        // 3.按照不同的织入目标分别去按织入Aspect的逻辑
//        if (ValidationUtil.isEmpty(categorizedMap)){
//            return;
//        }
//        for (Class<? extends Annotation>category : categorizedMap.keySet()){
//            weaveByCategory(category,categorizedMap.get(category));
//        }
//
//    }
//
//    private void weaveByCategory(Class<? extends Annotation> category, List<AspectInfo> aspectInfoList) {
//        // 1.获取被代理类的集合
//        Set<Class<?>> classSet=beanContainer.getClassesByAnnotation(category);
//        if(ValidationUtil.isEmpty(classSet)){
//            return;
//        }
//        // 2.遍历被代理类，分别为每个被代理类生成动态代理实例
//        for (Class<?> targetClass:classSet){
//            // 创建动态代理对象
//            AspectListExecutor aspectListExecutor=new AspectListExecutor(targetClass,aspectInfoList);
//            Object proxyBean=ProxyCreator.createProxy(targetClass,aspectListExecutor);
//            // 3.将动态代理对象实例添加到容器里，取代未代理前的类实例
//            beanContainer.addBean(targetClass,proxyBean);
//
//        }
//    }
//
//    /**
//     * 将切面类按照不同的织入目标进行切分
//     * @param categorizedMap
//     * @param aspectClass
//     */
//    private void categorizedAspect(Map<Class<? extends Annotation>, List<AspectInfo>> categorizedMap, Class<?> aspectClass) {
//        Order orderTag= aspectClass.getAnnotation(Order.class);
//        Aspect aspectTag=aspectClass.getAnnotation(Aspect.class);
//        DefaultAspect aspect= (DefaultAspect) beanContainer.getBean(aspectClass);
//
//        AspectInfo aspectInfo=new AspectInfo(orderTag.value(),aspect);
//
//        if(!categorizedMap.containsKey(aspectTag.value())){
//           //
//           List<AspectInfo> aspectInfoList=new ArrayList<>();
//           aspectInfoList.add(aspectInfo);
//           categorizedMap.put(aspectTag.value(),aspectInfoList);
//        }else {
//            List<AspectInfo> aspectInfoList=categorizedMap.get(aspectTag.value());
//            aspectInfoList.add(aspectInfo);
//        }
//
//
//    }
//
//    /**
//     * 框架中一定要遵循给Aspect类添加@Aspect和@Order标签的规范，同时，必须继承自DefaultAspect.class
//     * 此外，@Aspect的属性值不能是它本身
//     * @param aspectClass
//     * @return
//     */
//    private boolean verifyAspect(Class<?> aspectClass) {
//        return aspectClass.isAnnotationPresent(Aspect.class)&&
//                aspectClass.isAnnotationPresent(Order.class)&&
//                DefaultAspect.class.isAssignableFrom(aspectClass)&&
//                aspectClass.getAnnotation(Aspect.class).value()!=Aspect.class;
//    }
//}
