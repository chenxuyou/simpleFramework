package cn.airfei.controller.superadmin;

import cn.airfei.entity.bo.ShopCategory;
import cn.airfei.entity.dto.Result;
import cn.airfei.service.solo.IShopCategoryService;
import org.simpleFramework.core.annotation.Controller;
import org.simpleFramework.inject.annotation.Autowired;

import java.util.List;

/**
 * @description:
 * @author: air
 * @create: 2020-03-14 11:30
 */
@Controller
public class ShopCategoryOperationController {
    @Autowired
    private IShopCategoryService iShopCategoryService;


    public Result<Boolean> addShopCategory(ShopCategory shopCategory) {
        return iShopCategoryService.addShopCategory(shopCategory);
    }

    public Result<Boolean> removeShopCategory(ShopCategory shopCategory) {
        return iShopCategoryService.removeShopCategory(shopCategory);
    }

    public Result<Boolean> modifyShopCategory(ShopCategory shopCategory) {
        return iShopCategoryService.modifyShopCategory(shopCategory);
    }

    public Result<ShopCategory> getShopCategoryById(int shopCategoryId) {
        return iShopCategoryService.getShopCategoryById(shopCategoryId);
    }

    public Result<List<ShopCategory>> getShopCategoryList(ShopCategory shopCategoryCondition, int pageIndex, int pageSize) {
        return iShopCategoryService.getShopCategoryList(shopCategoryCondition, pageIndex, pageSize);
    }
    
    

}
