package cn.airfei.demo.pattern.proxy;

import cn.airfei.demo.pattern.proxy.cglib.AlipayMethodInterceptor;
import cn.airfei.demo.pattern.proxy.cglib.CglibUtil;
import cn.airfei.demo.pattern.proxy.impl.AlipayToC;
import cn.airfei.demo.pattern.proxy.impl.CommonPayment;
import cn.airfei.demo.pattern.proxy.impl.ToCPaymentImpl;
import cn.airfei.demo.pattern.proxy.jdkproxy.AlipayInvocationHandler;
import cn.airfei.demo.pattern.proxy.jdkproxy.JdkDynamicProxyUtil;
import net.sf.cglib.proxy.MethodInterceptor;

import java.lang.reflect.InvocationHandler;

/**
 * @description:
 * @author: air
 * @create: 2020-05-09 10:41
 */
public class ProxyDemo {

    public static void main(String[] args) {
//        ToCPayment toCProxy=new AlipayToC(new ToCPaymentImpl());
//
//        toCProxy.pay();

//        ToCPayment toCPayment=new ToCPaymentImpl();
//
//        InvocationHandler handler=new AlipayInvocationHandler(toCPayment);
//        ToCPayment toCProxy=JdkDynamicProxyUtil.newProxyInstance(toCPayment,handler);
//        toCProxy.pay();

        CommonPayment commonPayment=new CommonPayment();
        MethodInterceptor methodInterceptor=new AlipayMethodInterceptor();

        CommonPayment commonPaymentProxy= CglibUtil.createProxy(commonPayment,methodInterceptor);
        commonPaymentProxy.pay();

        ToCPayment toCPayment=new ToCPaymentImpl();

        ToCPayment toCPaymentProxy=CglibUtil.createProxy(toCPayment,methodInterceptor);

        System.out.println("for interface");
        toCPaymentProxy.pay();



    }

}
