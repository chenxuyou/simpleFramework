package cn.airfei.demo.pattern.proxy;

public interface ToCPayment {
    void pay();
}
