package cn.airfei.demo.pattern.callback;

/**
 * @description:
 * @author: air
 * @create: 2020-05-06 10:27
 */
public class CallBackDemo {

    public static void main(String[] args) {
        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("我要休息啦");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("我被回调啦");
            }
        });
        thread.start();
    }

}
