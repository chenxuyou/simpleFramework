package cn.airfei.demo.pattern.eventmode;

import lombok.Data;

/**
 * @description:
 * @author: air
 * @create: 2020-05-06 10:36
 */
@Data
public class Event {
    private String type;

}
