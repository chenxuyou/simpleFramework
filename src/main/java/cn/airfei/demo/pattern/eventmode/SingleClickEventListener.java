package cn.airfei.demo.pattern.eventmode;

/**
 * @description:
 * @author: air
 * @create: 2020-05-06 10:38
 */
public class SingleClickEventListener implements EventListener{


    @Override
    public void processEvent(Event event) {
        if ("singleclick".equals(event.getType())){
            System.out.println("单击事件被触发");
        }
    }
}
