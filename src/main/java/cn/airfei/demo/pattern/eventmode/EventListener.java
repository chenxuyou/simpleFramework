package cn.airfei.demo.pattern.eventmode;

public interface EventListener {
    void processEvent(Event event);
}
