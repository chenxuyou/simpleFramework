package cn.airfei.demo.pattern;


/**
 * @description:
 * @author: air
 * @create: 2020-03-16 10:38
 */
public class EnumStarvingSingleton {
    public static EnumStarvingSingleton getInstance(){
        return ContainerHolder.HOLDER.instance;
    }


    private EnumStarvingSingleton(){ }

    private enum  ContainerHolder{
        HOLDER ;
        private EnumStarvingSingleton instance;
        ContainerHolder(){
            instance=new EnumStarvingSingleton();
        }


    }

}
