package cn.airfei.entity.dto;

import cn.airfei.entity.bo.HeadLine;
import cn.airfei.entity.bo.ShopCategory;
import lombok.Data;

import java.util.List;

/**
 * @description:
 * @author: air
 * @create: 2020-03-14 10:29
 */
@Data
public class MainPageInfoDto {
    private List<HeadLine> headLineList;
    private List<ShopCategory> shopCategoryList;
}
