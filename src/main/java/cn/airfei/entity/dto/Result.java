package cn.airfei.entity.dto;

import lombok.Data;

/**
 * @description:
 * @author: air
 * @create: 2020-03-14 10:16
 */
@Data
public class Result<T> {
    private Integer code;
    private String msg;
    private T data;
}
