package cn.airfei.aspect;

import lombok.extern.slf4j.Slf4j;
import org.simpleFramework.aop.annotation.Aspect;
import org.simpleFramework.aop.annotation.Order;
import org.simpleFramework.aop.aspect.DefaultAspect;
import org.simpleFramework.core.annotation.Controller;

import java.lang.reflect.Method;

/**
 * @description:
 * @author: air
 * @create: 2020-05-11 10:41
 */
@Aspect(pointcut = "within(cn.airfei.controller.superadmin.*)")
@Order(value = 0)
@Slf4j
public class ControllerTimeCalculatorAspect extends DefaultAspect {
    private Long timestampCache;


    @Override
    public void before(Class<?> targetClass, Method method, Object[] args) throws Throwable {
        log.info("开始计时，执行的类是：{}，执行的方法是：{}，参数是：{}",targetClass.getName(),method,args);
        timestampCache=System.currentTimeMillis();
    }

    @Override
    public Object afterReturning(Class<?> targetClass, Method method, Object[] args, Object returnValue) throws Throwable {
        long endTime=System.currentTimeMillis();

        long costTime=endTime-timestampCache;
        log.info("开始计时，执行的类是：{}，执行的方法是：{}，参数是：{}，执行时间是：{}",targetClass.getName(),method,args,costTime);

        return returnValue;
    }
}
