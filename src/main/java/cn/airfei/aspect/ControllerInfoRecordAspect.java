package cn.airfei.aspect;

import lombok.extern.slf4j.Slf4j;
import org.simpleFramework.aop.annotation.Aspect;
import org.simpleFramework.aop.annotation.Order;
import org.simpleFramework.aop.aspect.DefaultAspect;
import org.simpleFramework.core.annotation.Controller;

import java.lang.reflect.Method;

/**
 * @description:
 * @author: air
 * @create: 2020-05-11 10:57
 */
@Slf4j
@Aspect(pointcut = "within(cn.airfei.controller.*)")
@Order(10)
public class ControllerInfoRecordAspect extends DefaultAspect {
    @Override
    public void before(Class<?> targetClass, Method method, Object[] args) throws Throwable {
        log.info("开始计时，执行的类是：{}，执行的方法是：{}，参数是：{}",targetClass.getName(),method,args);
        super.before(targetClass, method, args);
    }

    @Override
    public Object afterReturning(Class<?> targetClass, Method method, Object[] args, Object returnValue) throws Throwable {
        log.info("开始计时，执行的类是：{}，执行的方法是：{}，参数是：{}",targetClass.getName(),method,args);
        return super.afterReturning(targetClass, method, args, returnValue);
    }

    @Override
    public void afterThrowing(Class<?> targetClass, Method method, Object[] args, Throwable e) throws Throwable {
        log.info("开始计时，执行的类是：{}，执行的方法是：{}，参数是：{}",targetClass.getName(),method,args);
        super.afterThrowing(targetClass, method, args, e);
    }
}
