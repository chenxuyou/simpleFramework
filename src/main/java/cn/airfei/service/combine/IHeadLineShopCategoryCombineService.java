package cn.airfei.service.combine;

import cn.airfei.entity.dto.MainPageInfoDto;
import cn.airfei.entity.dto.Result;

public interface IHeadLineShopCategoryCombineService {
    Result<MainPageInfoDto> getMainPageInfo();
}
