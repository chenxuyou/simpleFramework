package cn.airfei.service.solo.impl;

import cn.airfei.entity.bo.HeadLine;
import cn.airfei.entity.dto.Result;
import cn.airfei.service.solo.IHeadLineService;
import lombok.extern.slf4j.Slf4j;
import org.simpleFramework.core.annotation.Service;

import java.util.List;

/**
 * @description:
 * @author: air
 * @create: 2020-03-14 10:27
 */
@Service
@Slf4j
public class HeadLineServiceImpl implements IHeadLineService {

    public Result<Boolean> addHeadLine(HeadLine headLine) {
        log.info("addHeadLine 被执行了");
        return null;
    }

    public Result<Boolean> removeHeadLine(HeadLine headLine) {
        return null;
    }

    public Result<Boolean> modifyHeadLine(HeadLine headLine) {
        return null;
    }

    public Result<HeadLine> getHeadLineById(int headLineId) {
        return null;
    }

    public Result<List<HeadLine>> getHeadLineList(HeadLine headLineCondition, int pageIndex, int pageSize) {
        return null;
    }
}
