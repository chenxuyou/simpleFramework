package cn.airfei.service.solo;

import cn.airfei.entity.bo.HeadLine;
import cn.airfei.entity.dto.Result;

import java.util.List;

public interface IHeadLineService {
    Result<Boolean> addHeadLine(HeadLine headLine);
    Result<Boolean> removeHeadLine(HeadLine headLine);
    Result<Boolean> modifyHeadLine(HeadLine headLine);

    Result<HeadLine> getHeadLineById(int headLineId);
    Result<List<HeadLine>> getHeadLineList(HeadLine headLineCondition,int pageIndex,int pageSize);

}
