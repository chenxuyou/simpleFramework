package cn.airfei;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @description:
 * @author: air
 * @create: 2020-03-13 14:50
 */
@WebServlet(value = "/hello")
@Slf4j
public class HelloServlet extends HttpServlet {

    @Override
    public void init() throws ServletException {
        System.out.println("初始化信息");
    }

    @Override
    public void destroy() {
        System.out.println("destroy...");
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("是我执行了doget方法，我才是入口");
        doGet(req,resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("logdsfsdfafa");
        String name = "my simple framework";
        req.setAttribute("name",name);
        System.out.println("hello");
        req.getRequestDispatcher("WEB-INF/jsp/hello.jsp").forward(req,resp);
    }


}
