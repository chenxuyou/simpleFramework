package org.simpleFramework.util.core;

import cn.airfei.controller.frontend.MainPageController;
import cn.airfei.entity.bo.HeadLine;
import org.junit.jupiter.api.*;
import org.simpleFramework.core.BeanContainer;
import org.simpleFramework.core.annotation.Controller;
import org.simpleFramework.core.annotation.Service;

/**
 * @description:
 * @author: air
 * @create: 2020-03-16 11:55
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class BeanContainerTest {
    private static BeanContainer beanContainer;

    @BeforeAll
    static void init(){
        System.out.println("init");
        beanContainer=BeanContainer.getInstance();
    }

    @Test
    @DisplayName(value = "loadBeans")
    @Order(1)
    public void loadBeans(){
        Assertions.assertEquals(false,beanContainer.isLoading());

        beanContainer.loadBeans("cn.airfei");
        Assertions.assertEquals(6,beanContainer.size());
        Assertions.assertEquals(true,beanContainer.isLoading());
    }

    @Order(4)
    @DisplayName("getBean")
    @Test
    public void getBean(){
        MainPageController mainPageController= (MainPageController) beanContainer.getBean(MainPageController.class);
        Assertions.assertEquals(true,mainPageController instanceof MainPageController);

        HeadLine headLine= (HeadLine) beanContainer.getBean(HeadLine.class);
        Assertions.assertEquals(null,headLine);
    }


    @Test
    @Order(3)
    @DisplayName(value = "getClassesByAnnotation")
    public void getClassesByAnnotation(){
        System.out.println(beanContainer.getClassesByAnnotation(Controller.class).size());
        Assertions.assertEquals(3,beanContainer.getClassesByAnnotation(Service.class).size());
    }



}
