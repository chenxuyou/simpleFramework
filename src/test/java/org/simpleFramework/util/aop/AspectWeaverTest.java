package org.simpleFramework.util.aop;

import cn.airfei.controller.superadmin.HeadLineOperationController;
import org.junit.jupiter.api.Test;
import org.simpleFramework.aop.AspectWeaver;
import org.simpleFramework.core.BeanContainer;
import org.simpleFramework.inject.DependencyInject;

/**
 * @description:
 * @author: air
 * @create: 2020-05-11 10:38
 */
public class AspectWeaverTest {

    @Test
    public void doAop(){
        BeanContainer beanContainer=BeanContainer.getInstance();
        beanContainer.loadBeans("cn.airfei");

        new AspectWeaver().doAop();

        new DependencyInject().doIoc();
        HeadLineOperationController headLineOperationController= (HeadLineOperationController) beanContainer.getBean(HeadLineOperationController.class);


        headLineOperationController.addHeadLine(null);
        headLineOperationController.getHeadLineById(1);



    }

}
