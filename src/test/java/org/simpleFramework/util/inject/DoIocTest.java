package org.simpleFramework.util.inject;

import cn.airfei.controller.frontend.MainPageController;
import org.junit.jupiter.api.*;
import org.simpleFramework.core.BeanContainer;
import org.simpleFramework.inject.DependencyInject;

/**
 * @description:
 * @author: air
 * @create: 2020-03-16 15:51
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class DoIocTest {

    @Test
    @DisplayName("doIoc")
    public void doIoc(){
        BeanContainer beanContainer=BeanContainer.getInstance();

        beanContainer.loadBeans("cn.airfei");
        Assertions.assertEquals(true,beanContainer.isLoading());

        MainPageController mainPageController= (MainPageController) beanContainer.getBean(MainPageController.class);
        Assertions.assertEquals(true,mainPageController instanceof MainPageController);

        Assertions.assertEquals(null,mainPageController.getIHeadLineShopCategoryCombineService());

        new DependencyInject().doIoc();
        Assertions.assertNotEquals(null,mainPageController.getIHeadLineShopCategoryCombineService());






    }

}
